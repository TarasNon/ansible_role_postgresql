Ansible Role: Postgresql
=========

Installs and configures PostgreSQL server

Requirements
------------

No special requirements

Role Variables
--------------

Deafults
[defaults/main.yml](defaults/main.yml)

Dependencies
------------

None

Example Playbook
----------------
    - name: Setup DB
      hosts: db
      become: true
      gather_facts: true
      roles:
        - ansible_role_postgresql

License
-------

MIT

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
